//import express from 'express';
const { request } = require('express');
const express = require('express');
const { BASE_URL,BASE_URL_V2,personasPATH } = require('./routes');

const response = {
    sendFile: function (file) {
        console.log(file);
    }
};

const PORT = 3000;
const personas = [
    {
        _id:1,
        nombre: 'Morales Paredes Jean Pierre',
        edad: 23,
        job: 'Analista pogramador'
    },
    {
        _id:2,
        nombre: 'PEDRO PARKER',
        edad: 25,
        job: 'ADMINISTRADOR'
    },
    {
        _id:3,
        nombre: 'RAUL GONZALES',
        edad: 24,
        job: 'ABOGADO'
    }
];

const app = express();

app.use(express.json());

app.get('/', (request, response) =>{
    return response.sendFile(__dirname.concat('/index.html'), err => {
        if(err){
            console.error(err);
        }
    });
});

app.get('/testjson', (request, response) =>{
    const body = {
        saludo: 'Primera ruta con express'
    };
    response.send(body);
});

app.get('/api/personas',(_,response) =>{
    response.send(personas);
});
app.get(`${BASE_URL}${personasPATH}`,(request,response) =>{
    //request.body.nombre;
    //request.body.edad;
    //request.body.job;
    try {
        const { nombre, edad, job } = request.body;
        if( nombre && edad && job){

            const nuevaPersona ={
                _id: personas.length +1,
                nombre,
                edad,
                job
            }
            personas.push(nuevaPersona);
            return response.status(201).send({ message: 'Persona agregada  correctamente'});
        }
        
        return response.status(403).send({
            message: 'Datos Incorrectos',
            nombre,
            edad,
            job
        });

    } catch (e) {
        response.status(500).send(
            {error: JSON.stringify(e)}
        );
        return;
    }

});

app.patch(`${BASE_URL}${personasPATH}`, (request,response) => {
    try {
        //const { nombre, edad, job, _id } = request.body;
        const persona = request.body;
        //if( nombre && edad && job && _id){
        if( persona._id){

            const personaIndex = personas.findIndex(value => value._id === persona._id);
            if(personaIndex > -1){
                //CASO ESPECIFICO
                //const persona = request.body;
                //personas[personaIndex] = {...persona[personaIndex], nombre}

                //personas[personaIndex] = {nombre,edad,job, _id: personas[personaIndex]._id};
                personas[personaIndex] = {...persona, _id: personas[personaIndex]._id};
                return response.status(201).send({ message: 'Persona actualizada correctamente'});
            }
            response.status(400).send({ message: 'No se encontró coincidencias'});
        }
        return response.status(403).send({ message: 'faltan campos.'});

    } catch (error) {
        response.status(500).send(
            {message: 'Ha ocurrido un error en el servidor.'}
        );
        return;
    }
});

app.delete(`${BASE_URL}${personasPATH}`, (request,response) => {
    try {
        //const { nombre, edad, job, _id } = request.body;
        const persona = request.body;
        //if( nombre && edad && job && _id){
        if( persona._id){

            const personaIndex = personas.findIndex(value => value._id === persona._id);
            if(personaIndex > -1){
                //CASO ESPECIFICO
                //const persona = request.body;
                //personas[personaIndex] = {...persona[personaIndex], nombre}

                //personas[personaIndex] = {nombre,edad,job, _id: personas[personaIndex]._id};
                personas.splice(personaIndex,1);
                return response.status(201).send({ message: 'Persona eliminada correctamente'});
            }
            response.status(400).send({ message: 'No se encontró coincidencias'});
        }
        return response.status(403).send({ message: 'faltan campos.'});

    } catch (error) {
        response.status(500).send(
            {message: 'Ha ocurrido un error en el servidor.'}
        );
        return;
    }
});

app.listen(PORT, () => {
    console.log('Aplicacion ejecutando en el puerto 3000, en http://localhost:3000');
});

console.log(`the application  is running on http://localhost:${PORT}`);